<?php

namespace Codeception\Module;

use Codeception\Exception\ModuleConfigException;
use Codeception\Module;
use Drupal\Driver\DrupalDriver;

/**
 * Bootstrap Drupal to the drupal API can be used.
 */
class DrupalBootstrap extends Module
{
    /**
     * @var DrupalDriver
     */
    protected $driver;

    /**
     * Constructor. Receive and store module config.
     *
     * @throws \Codeception\Exception\ModuleConfigException
     */
    public function _initialize()
    {
        parent::_initialize();

        if (empty($this->config['drupal_root'])) {
            throw new ModuleConfigException("DrupalBootstrap", "drupal_root not set.");
        }

        if (empty($this->config['drupal_root'])) {
            throw new ModuleConfigException("DrupalBootstrap", "drupal_uri not set.");
        }

        if (!file_exists($this->config['drupal_root'])) {
            throw new ModuleConfigException(
                "DrupalBootstrap",
                "drupal_root does not exist." . realpath(getcwd()) . "/" . $this->config['drupal_root']
            );
        }

        $this->driver = new DrupalDriver($this->config['drupal_root'], $this->config['drupal_uri']);
        $this->driver->setCoreFromVersion();

        // Bootstrap Drupal.
        $this->driver->bootstrap();
    }

    /**
     * @return array
     */
    public static function getRequiredFields()
    {
        return array("drupal_root", "drupal_uri");
    }

    /**
     * @return \Drupal\Driver\DrupalDriver
     */
    public function getDrupalDriver()
    {
        return $this->driver;
    }
}
