<?php
use Codeception\Module\DrupalBootstrap;

/**
 * Created by PhpStorm.
 * User: arigby
 * Date: 23/06/2016
 * Time: 15:08
 */

namespace Codeception\Module;

use Codeception\Module;
use Drupal\Driver\DrupalDriver;

/**
 * Class DrupalContent
 * @package Codeception\Module
 */
class DrupalContent extends Module
{
    /**
     * @var DrupalDriver
     */
    protected $driver;

    public function _initialize()
    {
        /* @var DrupalBootstrap $module */
        $module = $this->getModule("DrupalBootstrap");
        $this->driver = $module->getDrupalDriver();
    }

    public function createUser(\stdClass $user)
    {
        $this->driver->userCreate($user);
    }

    public function deleteUser(\stdClass $user)
    {
        $this->driver->userDelete($user);
    }

    public function processBatch()
    {
        $this->driver->processBatch();
    }

    public function userAddRole(\stdClass $user, $role)
    {
        $this->driver->userAddRole($user, $role);
    }

    public function fetchWatchdog($count = 10, $type = null, $severity = null)
    {
        return $this->driver->fetchWatchdog($count, $type, $severity);
    }

    public function clearCache($type = null)
    {
        $this->driver->clearCache($type);
    }

    public function clearStaticCaches()
    {
        return $this->clearStaticCaches();
    }

    public function createNode($node)
    {
        return $this->driver->createNode($node);
    }

    public function deleteNode($node)
    {
        return $this->driver->nodeDelete($node);
    }

    public function runCron()
    {
        $this->driver->runCron();
    }

    public function createTerm(\stdClass $term)
    {
        return $this->driver->createTerm($term);
    }

    public function deleteTerm(\stdClass $term)
    {
        return $this->driver->termDelete($term);
    }

    public function createRole(array $permissions)
    {
        return $this->driver->roleCreate($permissions);
    }

    public function deleteRole($rid)
    {
        $this->driver->roleDelete($rid);
    }

    public function isField($entity_type, $field_name)
    {
        return $this->driver->isField($entity_type, $field_name);
    }

    public function configGet($name, $key)
    {
        return $this->driver->configGet($name, $key);
    }

    public function configSet($name, $key, $value)
    {
        $this->driver->configSet($name, $key, $value);
    }
}
