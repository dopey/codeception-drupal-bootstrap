#Drupal Bootstrap

[Codeception](http://www.codeception.com) module for bootstrapping [Drupal](http://drupal.org) to access the api.

#Requirements

* Drupal 7

#Install

Install using composer:

```
"require": {
     "ixis/codeception-drupal-bootstrap": "dev-develop"
   }
```
#Configure

Add 'DrupalBootstrap' module to the suite configuration.

```
class_name: AcceptanceTester
modules:
    enabled:
        - DrupalBootstrap
```

##Module configuration

* 'drupal_site_path' - path to drupal site dir. This should include settings.php.
